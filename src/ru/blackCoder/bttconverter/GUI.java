package ru.blackCoder.bttconverter;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by BlackCoder on 03.03.2016.
 */
public class GUI extends JFrame
{
    JTextField fld_from;

    JTextArea area_to;
    JScrollPane scroll_to;

    JButton btn_btt;
    JButton btn_ttb;

    public GUI() {
        setTitle("Bin to Text - by BlackCoder - v0.1");
        setSize(500, 335);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLayout(null);
        setLocationRelativeTo(null);
        setResizable(false);

        initComponents();
        addListeners();
        addComponents();

        setVisible(true);
    }

    private void initComponents()
    {
        fld_from = new JTextField();
        fld_from.setBounds(5, 5, 485, 25);

        area_to = new JTextArea();
        scroll_to = new JScrollPane(area_to);
        scroll_to.setBounds(5, 35, 485, 230);

        btn_btt = new JButton("BIN => TEXT");
        btn_btt.setBounds(5, 270, 240, 25);

        btn_ttb = new JButton("BIN <= TEXT");
        btn_ttb.setBounds(250, 270, 240, 25);
    }

    private void addListeners()
    {
        // No lambada => Support JRE 7
        btn_btt.addActionListener(
                new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent e)
                    {
                        // Лень думать над проверками, по-этому тут будет обработчик искючений :3
                        // Костыли :3
                        try {
                            area_to.setText("");
                            for (int i = 0; i <= fld_from.getText().length() - 8; i += 8) {
                                int k = Integer.parseInt(fld_from.getText().substring(i, i + 8), 2);
                                area_to.setText(area_to.getText() + (char) k);
                            }
                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(null, "Возникла ошибка при попытке конвертировать BIN => TEXT\nStackTrace: " + ex.getStackTrace()[0] + "\n Более подробная информация об ошибке доступна на stackoverflow.com", "Error #1", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }
        );

        btn_ttb.addActionListener(
                new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent e)
                    {
                        // Работаем тем-же принципом, что и выше...
                        // Думаю объяснять что тут происходит, не стоит...
                        try {
                            byte[] bytes = area_to.getText().getBytes();
                            StringBuilder binary = new StringBuilder();
                            for (byte b : bytes) {
                                int val = b;
                                for (int i = 0; i < 8; i++) {
                                    binary.append((val & 128) == 0 ? 0 : 1);
                                    val <<= 1;
                                }
                            }
                            fld_from.setText(binary.toString());
                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(null, "Возникла ошибка при попытке конвертировать TEXT => BIN\nStackTrace: " + ex.getStackTrace()[0] + "\n Более подробная информация об ошибке доступна на stackoverflow.com", "Error #2", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }
        );
    }

    private void addComponents()
    {
        add(fld_from);
        add(scroll_to);
        add(btn_btt);
        add(btn_ttb);
    }
}